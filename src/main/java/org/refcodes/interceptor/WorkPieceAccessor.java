// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.interceptor;

/**
 * Provides an accessor for a work piece property.
 *
 * @param <WP> The type of the work piece.
 */
public interface WorkPieceAccessor<WP extends Object> {

	/**
	 * Gets the work piece.
	 *
	 * @return the work piece
	 */
	WP getWorkPiece();

	/**
	 * Provides a mutator for a work piece property.
	 * 
	 * @param <WP> The type of the work piece.
	 */
	public interface WorkPieceMutator<WP extends Object> {

		/**
		 * Sets the work piece.
		 *
		 * @param aWorkPiece the new work piece
		 */
		void setWorkPiece( WP aWorkPiece );
	}

	/**
	 * Provides a work piece property.
	 * 
	 * @param <WP> The type of the work piece.
	 */
	public interface WorkPieceProperty<WP extends Object> extends WorkPieceAccessor<WP>, WorkPieceMutator<WP> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setWorkPiece(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aWorkPiece The value to set (via
		 *        {@link #setWorkPiece(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default WP letWorkPiece( WP aWorkPiece ) {
			setWorkPiece( aWorkPiece );
			return aWorkPiece;
		}
	}
}
