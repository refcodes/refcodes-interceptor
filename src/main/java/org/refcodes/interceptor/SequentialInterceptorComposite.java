// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.interceptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.refcodes.component.Component;
import org.refcodes.component.ComponentComposite;
import org.refcodes.component.ComponentUtility;
import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.controlflow.ExecutionStrategy;

/**
 * A compound sequential interceptor which is passing the work piece from one
 * contained interceptor to the next in the order in which them were passed. In
 * case any of the contained interceptors returns flags the processing of the
 * work piece to be "finished" (in its {@link #intercept(Object)} method it
 * returns true), then this compound sequential interceptor returns true.
 * <p>
 *
 * @param <WP> The work piece which is being passed to the contained
 *        interceptors one after the other and which is processed by the
 *        interceptors.
 */
public class SequentialInterceptorComposite<WP> implements Interceptor<WP>, ComponentComposite {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutionStrategy _componentExecutionStrategy;

	private boolean _isContinueOnFinished;

	private boolean _isContinueOnError;

	private List<Interceptor<WP>> _interceptors;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTIR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the sequential interceptor which contains a list of
	 * interceptors being invoked in the order in which them were added upon
	 * calling the method {@link #intercept(Object)}.
	 * 
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public SequentialInterceptorComposite( Interceptor<WP>... aInterceptors ) {
		this( ExecutionStrategy.JOIN, false, false, aInterceptors );
	}

	/**
	 * Constructs the sequential interceptor which contains a list of
	 * interceptors being invoked in the order in which them were added upon
	 * calling the method {@link #intercept(Object)}.
	 * 
	 * @param aComponentExecutionStrategy The strategy on how to invoke the
	 *        state change requests (as of the {@link ComponentComposite})
	 *        defined methods) on the herein contained {@link Component}
	 *        instances. CAUTION: The strategy does not affect on how the
	 *        {@link #intercept(Object)} methods of the herein contained
	 *        instances are invoked.
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public SequentialInterceptorComposite( ExecutionStrategy aComponentExecutionStrategy, Interceptor<WP>... aInterceptors ) {
		this( aComponentExecutionStrategy, false, false, aInterceptors );
	}

	/**
	 * Constructs the sequential interceptor which contains a list of
	 * interceptors being invoked in the order in which them were added upon
	 * calling the method {@link #intercept(Object)}. The sequential
	 * interceptor can be configured to continue the processing sequence even in
	 * case a {@link WorkPieceException} was thrown or even if a containing
	 * interceptor signaled "finished" after processing the work piece (by
	 * returning true" after invocation of the method
	 * {@link #intercept(Object)} ).
	 * 
	 * @param isContinueOnFinished True in case all subsequent interceptors are
	 *        to be processed even if an interceptor signaled "finished" after
	 *        processing the work piece.
	 * @param isContinueOnError True in case all subsequent interceptors are to
	 *        be processed even if an interceptor signaled an error after
	 *        processing the work piece (by throwing an
	 *        {@link WorkPieceException}).
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public SequentialInterceptorComposite( boolean isContinueOnFinished, boolean isContinueOnError, Interceptor<WP>... aInterceptors ) {
		this( ExecutionStrategy.JOIN, isContinueOnFinished, isContinueOnError, aInterceptors );
	}

	/**
	 * Constructs the sequential interceptor which contains a list of
	 * interceptors being invoked in the order in which them were added upon
	 * calling the method {@link #intercept(Object)}. The sequential
	 * interceptor can be configured to continue the processing sequence even in
	 * case a {@link WorkPieceException} was thrown or even if a containing
	 * interceptor signaled "finished" after processing the work piece (by
	 * returning true" after invocation of the method
	 * {@link #intercept(Object)} ).
	 * 
	 * @param aComponentExecutionStrategy The strategy on how to invoke the
	 *        state change requests (as of the {@link ComponentComposite})
	 *        defined methods) on the herein contained {@link Component}
	 *        instances. CAUTION: The strategy does not affect on how the
	 *        {@link #intercept(Object)} methods of the herein contained
	 *        instances are invoked.
	 * @param isContinueOnFinished True in case all subsequent interceptors are
	 *        to be processed even if an interceptor signaled "finished" after
	 *        processing the work piece.
	 * @param isContinueOnError True in case all subsequent interceptors are to
	 *        be processed even if an interceptor signaled an error after
	 *        processing the work piece (by throwing an
	 *        {@link WorkPieceException}).
	 * @param aInterceptors The interceptors to be invoked by the compound
	 *        sequential interceptor.
	 */
	@SafeVarargs
	public SequentialInterceptorComposite( ExecutionStrategy aComponentExecutionStrategy, boolean isContinueOnFinished, boolean isContinueOnError, Interceptor<WP>... aInterceptors ) {
		_isContinueOnFinished = isContinueOnFinished;
		_isContinueOnError = isContinueOnError;
		_interceptors = Arrays.asList( aInterceptors );
		_componentExecutionStrategy = aComponentExecutionStrategy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean intercept( WP aWorkPiece ) throws WorkPieceException {
		boolean isFinished = false;
		for ( Interceptor<WP> eInterceptor : _interceptors ) {
			try {
				if ( eInterceptor.intercept( aWorkPiece ) ) {
					isFinished = true;
				}
				if ( !_isContinueOnFinished && isFinished ) {
					return true;
				}
			}
			catch ( WorkPieceException e ) {
				if ( !_isContinueOnError ) {
					throw e;
				}
			}
		}
		return isFinished;
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		ComponentUtility.initialize( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		ComponentUtility.start( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		ComponentUtility.pause( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		ComponentUtility.resume( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		ComponentUtility.stop( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		ComponentUtility.decompose( _componentExecutionStrategy, _interceptors );
		_interceptors.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		ComponentUtility.flush( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		ComponentUtility.destroy( _componentExecutionStrategy, _interceptors );
		_interceptors.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		ComponentUtility.reset( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		ComponentUtility.open( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		ComponentUtility.close( _componentExecutionStrategy, _interceptors );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ComponentUtility.dispose( _componentExecutionStrategy, _interceptors );
	}
}