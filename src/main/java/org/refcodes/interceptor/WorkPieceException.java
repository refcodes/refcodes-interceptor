// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.interceptor;

/**
 * The work piece exception is related to a work piece. Therefore the work piece
 * exception provides access to the work piece in question.
 */
public class WorkPieceException extends InterceptorException implements WorkPieceAccessor<Object> {

	private static final long serialVersionUID = 1L;

	private Object _workPiece;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public WorkPieceException( Object aWorkPiece, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_workPiece = aWorkPiece;
	}

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public WorkPieceException( Object aWorkPiece, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_workPiece = aWorkPiece;
	}

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public WorkPieceException( Object aWorkPiece, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_workPiece = aWorkPiece;
	}

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aMessage The aMessage describing this exception.
	 */
	public WorkPieceException( Object aWorkPiece, String aMessage ) {
		super( aMessage );
		_workPiece = aWorkPiece;
	}

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public WorkPieceException( Object aWorkPiece, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_workPiece = aWorkPiece;
	}

	/**
	 * Instantiates a new work piece exception.
	 *
	 * @param aWorkPiece the work piece
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public WorkPieceException( Object aWorkPiece, Throwable aCause ) {
		super( aCause );
		_workPiece = aWorkPiece;
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getWorkPiece() {
		return _workPiece;
	}
}
