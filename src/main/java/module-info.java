module org.refcodes.interceptor {
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;

	exports org.refcodes.interceptor;
}
