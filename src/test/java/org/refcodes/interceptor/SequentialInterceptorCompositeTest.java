// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.interceptor;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SequentialInterceptorCompositeTest {

	/**
	 * Tests the compound sequential interceptor in default configuration.
	 *
	 * @throws WorkPieceException the work piece exception
	 */
	@Test
	public void testExitOnAll() throws WorkPieceException {
		AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		Result theResult = new Result();
		SequentialInterceptorComposite<Result> theCompoundSequentialInterceptor = new SequentialInterceptorComposite<Result>( false, false, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 2, theResult.getResult() );
	}

	/**
	 * Tests the compound sequential interceptor in default configuration.
	 *
	 * @throws WorkPieceException the work piece exception
	 */
	@Test
	public void testContinueOnFinished() throws WorkPieceException {
		AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		Result theResult = new Result();
		SequentialInterceptorComposite<Result> theCompoundSequentialInterceptor = new SequentialInterceptorComposite<Result>( true, false, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 6, theResult.getResult() );
	}

	/**
	 * Tests the compound sequential interceptor in default configuration.
	 */
	@Test
	public void testFaliOnError() {
		AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		DivInterceptor theDivInterceptor = new DivInterceptor( 0 );
		Result theResult = new Result();
		SequentialInterceptorComposite<Result> theCompoundSequentialInterceptor = new SequentialInterceptorComposite<Result>( false, false, theDivInterceptor, theAddInterceptor, theAddInterceptor, theAddInterceptor );
		try {
			theCompoundSequentialInterceptor.intercept( theResult );
			fail( "Should not reach this code!" );
		}
		catch ( WorkPieceException e ) {
			/* expected */
		}
	}

	/**
	 * Tests the compound sequential interceptor in default configuration.
	 *
	 * @throws WorkPieceException the work piece exception
	 */
	@Test
	public void testContinueOnAll() throws WorkPieceException {
		AddInterceptor theAddInterceptor = new AddInterceptor( 2 );
		DivInterceptor theDivInterceptor = new DivInterceptor( 0 );
		Result theResult = new Result();
		SequentialInterceptorComposite<Result> theCompoundSequentialInterceptor = new SequentialInterceptorComposite<Result>( true, true, theAddInterceptor, theDivInterceptor, theAddInterceptor, theAddInterceptor );
		theCompoundSequentialInterceptor.intercept( theResult );
		assertEquals( 6, theResult.getResult() );
	}

	// //////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// //////////////////////////////////////////////////////////////////////////

	/**
	 * An interceptor for adding a summand to a result.
	 */
	public class AddInterceptor implements Interceptor<Result> {

		private int _summand;

		/**
		 * Instantiates a new adds the interceptor.
		 *
		 * @param aSummand the summand
		 */
		public AddInterceptor( int aSummand ) {
			_summand = aSummand;
		}

		/**
		 * Do intercept.
		 *
		 * @param aWorkPiece the work piece
		 * 
		 * @return true, if successful
		 * 
		 * @throws WorkPieceException the work piece exception
		 */
		@Override
		public boolean intercept( Result aWorkPiece ) throws WorkPieceException {
			aWorkPiece.setResult( aWorkPiece.getResult() + _summand );
			return true;
		}
	}

	/**
	 * An interceptor for dividing a result by a divisor.
	 */
	public class DivInterceptor implements Interceptor<Result> {

		private int _divisor;

		/**
		 * Instantiates a new div interceptor.
		 *
		 * @param aDivisor the divisor
		 */
		public DivInterceptor( int aDivisor ) {
			_divisor = aDivisor;
		}

		/**
		 * Do intercept.
		 *
		 * @param aWorkPiece the work piece
		 * 
		 * @return true, if successful
		 * 
		 * @throws WorkPieceException the work piece exception
		 */
		@Override
		public boolean intercept( Result aWorkPiece ) throws WorkPieceException {

			try {
				aWorkPiece.setResult( aWorkPiece.getResult() / _divisor );
			}
			catch ( ArithmeticException e ) {
				throw new WorkPieceException( aWorkPiece, e.getMessage(), e );
			}
			return true;
		}
	}

	/**
	 * The working object of the interceptor storing a result.
	 */
	public class Result {

		private int _result = 0;

		/**
		 * Gets the result.
		 * 
		 * @return The result.
		 */
		public int getResult() {
			return _result;
		}

		/**
		 * Sets the result.
		 * 
		 * @param aResult The result to be set.
		 */
		public void setResult( int aResult ) {
			_result = aResult;
		}
	}
}
