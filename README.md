# README #

> "*The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

## What is this repository for? ##

*** With the `refcodes-interceptor` artifact you can build up assembly lines of any structure; digesting and refining messages (objects) you pass in. In its most basic form, the `refcodes-interceptor` acts as an `interceptor` (as of the [interceptor pattern](https://en.wikipedia.org/wiki/Interceptor_pattern)).***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-interceptor</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.1.2</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-interceptor). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-interceptor).

## Introduction ##

The `interceptor pattern` is used when an operation's sequential execution may differ depending on the operation's incoming messages. It chains software components in a sequence - one after the other. This makes it suitable inside a [front controller pattern](https://en.wikipedia.org/wiki/Front_Controller_pattern). Here the messages are the HTTP-Requests. 

The `refcodes-interceptor` artifact provides you with different kinds of interceptors - in the context of the `refcodes-interceptor` artifact an `interceptor` is called `interceptor`: The composite `interceptor(s)` and the atomic interceptors `interceptor(s)`: 

An atomic `interceptor` is a plain `interceptor` doing its work. A composite `interceptor` contains other `interceptor`, being composite or atomic `interceptor`. Having those build blocks, you can nest composite `interceptor(s)` inside composite `interceptor(s)` containing atomic `interceptor(s)` setting up something like `runlevel` chains of execution. You can also just use one composite `interceptor` containing atomic `interceptors)` - setting up your individual `interceptor` chain.

As the `refcodes-interceptor` artifact is generic, you can build up your tailored object assembly lines digesting any kind of type.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-interceptor/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
